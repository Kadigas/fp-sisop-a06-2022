#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h> 
#include <dirent.h>
#include <syslog.h>
#define PORT 8080
#define BUFF 1024
#define SIZE 100

int valread;
char *users_file = "users.txt", *database = "problems.tsv", *db_folder="./databases/";
char *user_db = "./databases/user.db";
char userlog[SIZE]={};
int server_fd;
struct sockaddr_in address;
int opt = 1, new_socket;
int addrlen = sizeof(address);

char folderDatabase[] = "/home/kadigas/modul5/database/databases/";
char fileUser[] = "/home/kadigas/modul5/database/databases/user.db";
char currentDB[BUFF] = {0};
int isRoot = 0;
int acc = 0;

struct login {
    char id[BUFF];
    char password[BUFF];
} login;

void writelog(char *cmd, char *uname){
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char line[1000];

	FILE *file;
	file = fopen("/home/kadigas/modul5/database/databases/user_query.log", "ab");

	sprintf(line, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, uname, cmd);
	// sprintf(line, "%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, uname, filepath);

	fputs(line, file);
	fclose(file);
	return;
}

void mk_folder(char fdname[])
{
    char* dirname;
    dirname = malloc(strlen(fdname) + 1);
    strcpy(dirname, fdname);
    mkdir(dirname,0777);
}

void create_file(char fileName[], char str[], char mode[])
{
    FILE* file = fopen(fileName, mode);
    fprintf(file, "%s\n", str);
    fclose(file);
}

int auth(char str[])
{
    FILE* file = fopen(fileUser, "r+");

    char line[BUFF];
    while (fgets(line, BUFF, file))
    {
        if (!strcmp(line, str))
        {
            fclose(file);
            return 1;
        }
    }
    fclose(file);

    return 0;
}

void reconnect()
{
    char buffer[BUFF] = {0}, msg[BUFF] = {0};
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    // ambil uid
    valread = read(new_socket, buffer, BUFF);

    // root user
    if (!strcmp(buffer, "0"))
    {
        strcpy(msg, "Connection Accepted. Connected As Root!");
        strcpy(login.id, "root");
        acc = 1;
        isRoot = 1;
    }

    if (!acc)
    {
        // handle read di client
        send(new_socket, "TESTT", 10, 0);
        bzero(buffer, BUFF);

        valread = read(new_socket, buffer, BUFF);

        if (auth(buffer))
        {
            char strbackup[BUFF];
            strcpy(strbackup, buffer);
            char* ptr = strbackup;
            char* token;

            int i;
            for (i = 0; token = strtok_r(ptr, ":", &ptr); i++)
            {
                if (i == 0)
                {
                    strcpy(login.id, token);
                }
                else if (i == 1)
                {
                    strcpy(login.password, token);
                }
            }
            acc = 1;
            strcpy(msg, "Connection Accepted. Welcome ");
			strcat(msg, login.id);
        }
        else
        {
            strcpy(msg, "Invalid Username or Password!");
        }
    }
    send(new_socket, msg, strlen(msg), 0);
}

char* create_user(char str[])
{
    char* ptr;
    char msg[BUFF];
    bzero(msg, BUFF);

    char newuserid[BUFF];
    bzero(newuserid, BUFF);
    char newuserpass[BUFF];
    bzero(newuserpass, BUFF);

    char cmd[BUFF];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;
    int i;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
    {
        if (i == 2)
        {
            strcpy(newuserid, token);
        }
        else if (i == 5)
        {
            strncpy(newuserpass, token, strlen(token) - 1);
        }
        else if ((i == 3 && strcmp(token, "IDENTIFIED")) || (i == 4 && strcmp(token, "BY")) || i > 5)
        {
			strcpy(msg, "SYNTAX ERROR!");
            ptr = msg;
            return ptr;
        }
    }

    bzero(cmd, BUFF);
	strcpy(cmd, newuserid); strcat(cmd, ":"); strcat(cmd, newuserpass);
    create_file(fileUser, cmd, "a+");
    strcpy(msg, "CREATE USER SUCCESS!");

    writelog(str, login.id);
    ptr = msg;
    return ptr;
}

char* use(char str[])
{
    char* ptr;
    char msg[BUFF];
    bzero(msg, BUFF);

    char* dbptr = str + 4;
    char dbName[BUFF];
    bzero(dbName, BUFF);
    strncpy(dbName, dbptr, strlen(dbptr) - 1);

    char permissionFile[BUFF];
    bzero(permissionFile, BUFF);
    sprintf(permissionFile, "%s%s/granted_user.txt", folderDatabase, dbName);

    FILE* file = fopen(permissionFile, "r");
    if (!file)
    {
        strcpy(msg, "DATABASE NOT FOUND");
		ptr = msg;
		return ptr;
    }

    char line[BUFF];
    while (fgets(line, BUFF, file))
    {
        if (!strncmp(line, login.id, strlen(login.id)))
        {
            fclose(file);
            strcpy(currentDB, dbName);
            strcpy(msg, "SUCCESSFULLY ACCESS ");
            strcat(msg, currentDB);
            ptr = msg;
            writelog(str, login.id);
            return ptr;
        }
    }
    fclose(file);
    strcpy(msg, "ACCESS DATABASE DENIED");
    ptr = msg;
    return ptr;
}

char* grant_permission(char str[])
{
    char* ptr;
    char msg[BUFF];
    bzero(msg, BUFF);

    char dbName[BUFF];
    bzero(dbName, BUFF);

    char userid[BUFF];
    bzero(userid, BUFF);

    int i;
    char parse[BUFF];
    strcpy(parse, str);
    char* parseptr = parse;
    char* token;

    for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
    {
        if (i == 2)
        {
            strcpy(dbName, token);
        }
        else if (i == 4)
        {
            strncpy(userid, token, strlen(token) - 1);
        }
        else if (i == 3 && strcmp(token, "INTO"))
        {
            strcpy(msg, "SYNTAX ERROR!");
            ptr = msg;
            return ptr;
        }
    }

    FILE* file = fopen(fileUser, "r");
    char line[BUFF];
    int ada = 0;

    while (fgets(line, BUFF, file))
    {
        if (!strncmp(line, userid, strlen(userid)))
        {
            ada = 1;
            break;
        }
    }
    fclose(file);

    if (!ada)
    {
        strcpy(msg, "USER NOT FOUND!");
        ptr = msg;
        return ptr;
    }

    char namafile[BUFF];
    sprintf(namafile, "%s%s/granted_user.txt", folderDatabase, dbName);
    
    file = fopen(namafile, "a+");
    if (!file)
    {
        strcpy(msg, "DATABASE NOT FOUND");
        ptr = msg;
        return ptr;
    }
    fprintf(file, "%s\n", userid);
    fclose(file);

    strcpy(msg, "ACCESS GRANTED");
    ptr = msg;
    writelog(str, login.id);
    return ptr;
}

char* create_database(char str[])
{
	char* ptr;
	char msg[BUFF];

	char dbName[BUFF];
	bzero(dbName, BUFF);

	int i;
	char parse[BUFF];
	strcpy(parse, str);
	char* parseptr = parse;
	char* token;

	for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
	{
		if (i == 2)
		{
			strncpy(dbName, token, strlen(token) - 1);
		}
	}

	char dbPath[BUFF];
	sprintf(dbPath, "%s%s", folderDatabase, dbName);
	char* pathdbptr = dbPath;

	if (mkdir(pathdbptr, 0777) != 0)
	{
		strcpy(msg, "CANNOT CREATE DATABASE!");
		ptr = msg;
		return ptr;
	}

	char permissionfile[BUFF];
	strcpy(permissionfile, dbPath);
	strcat(permissionfile, "/granted_user.txt");

	create_file(permissionfile, login.id, "a+");

	strcpy(msg, "DATABASE SUCCESSFULLY CREATED!");
	ptr = msg;
    writelog(str, login.id);
	return ptr;
}

int remove_directory(const char *path) {
	DIR* d = opendir(path);
	size_t path_len = strlen(path);
	int r = -1;
	if (!d)
		return 1;
   	if (d) {
		struct dirent *p;

		r = 0;
		while (!r && (p=readdir(d))) {
          	int r2 = -1;
			char *buf;
			size_t len;
            
			if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
				continue;

			len = path_len + strlen(p->d_name) + 2; 
			buf = malloc(len);

			if (buf) {
				struct stat statbuf;

				snprintf(buf, len, "%s/%s", path, p->d_name);
				if (!stat(buf, &statbuf)) {
					if (S_ISDIR(statbuf.st_mode))
					r2 = remove_directory(buf);
					else
					r2 = unlink(buf);
				}
				free(buf);
			}
			r = r2;
		}
		closedir(d);
	}

	if (!r)
		r = rmdir(path);

	return r;
}


char* drop_database(char str[])
{
    writelog(str, login.id);
	char* ptr;
	char msg[BUFF];

	char dbName[BUFF];
	bzero(dbName, BUFF);

	int i;
	char parse[BUFF];
	strcpy(parse, str);
	char* parseptr = parse;
	char* token;

	for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
	{
		if (i == 2)
		{
			strncpy(dbName, token, strlen(token) - 1);
		}
	}

	char dbPath[BUFF];
	sprintf(dbPath, "%s%s", folderDatabase, dbName);
	char* pathdbptr = dbPath;

	if (!opendir(dbPath))
	{
		strcpy(msg, "DATABASE NOT EXIST!");
		ptr = msg;
		return ptr;
	}

	char permissionfile[BUFF];
	strcpy(permissionfile, dbPath);
	strcat(permissionfile, "/granted_user.txt");

	printf("%s\n", dbPath);

	FILE* file = fopen(permissionfile, "r");
	char line [BUFF];
	bzero(line, BUFF);
	int flag = 0;

	while (fgets(line, BUFF, file))
	{
		if (!strncmp(line, login.id, strlen(login.id)))
		{
			flag = 1;
			break;
		}
	}

	if (flag)
	{
		int rm = remove_directory(dbPath);
		strcpy(msg, "DATABASE DROPPED.");
		ptr = msg;
        writelog(str, login.id);
		return ptr;
	}

	strcpy(msg, "USER HAS NO PERMISSION!");
	ptr = msg;
    writelog(str, login.id);
	return ptr;
}

char* create_table(char str[])
{
	char* ptr;
	char msg[BUFF];

	char namatable[BUFF];
	
	int i = 0;
	char cmd[BUFF];
	strcpy(cmd, str);
	char* cmdptr = cmd;
	char* token;

	for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
	{
		if (i == 2)
		{
			strcpy(namatable, token);
		}
	}

	strcpy(cmd, strcpy);
    const char bracket = '(';
	char* cmdptr1 = strchr(str, bracket) + 1;

	strncpy(cmd, cmdptr1, strlen(cmdptr1) - 2);
	char temp[BUFF];
	strcpy(temp, cmd);
    
	char table_path[BUFF];
	sprintf(table_path, "%s%s/%s.tsv", folderDatabase, currentDB, namatable);

	FILE* file = fopen(table_path, "w+");
	char* token2;
	int j = 0;
	
	char kolomnama[BUFF], kolomdata[BUFF];
	for (i = 0; token = strtok_r(temp, ",", &temp); i++)
	{
		char kolom[BUFF];
		strcpy(kolom, token);
        char* kolomptr = kolom;
		for (j = 0; token2 = strtok_r(kolomptr, " ", &kolomptr); j++)
		{
			if (j == 0)
			{
				strcpy(kolomnama, token2);
			}
			if (j == 1)
			{
				strcpy(kolomdata, token2);
			}
		}
        fprintf(file, "%s:%s", kolomnama, kolomdata);
        fprintf(file, "\t");
	}
	fclose(file);

	strcpy(msg, "TABLE CREATED.");
	ptr = msg;
    writelog(str, login.id);
	return ptr;
}

char* drop_table(char str[]){
    char* ptr;
	char msg[BUFF];
 
	char namatable[BUFF];
 
	// temp
	int i = 0;
	char cmd[BUFF];
	strcpy(cmd, str);
	char* cmdptr = cmd;
	char* token;
 
	// DROP TABLE [nama_table];
	for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
	{
		if (i == 2)
		{
			strncpy(namatable, token, strlen(token) - 1);
		}
	}
 
	// cmd = [nama kolom] [tipe data], ...
	char table_path[BUFF];
	sprintf(table_path, "%s%s/%s.tsv", folderDatabase, currentDB, namatable);
 
	FILE* file = fopen(table_path, "r");
 
    char line [BUFF];
	bzero(line, BUFF);
	int flag = 0;
 
	int status;
	pid_t child_id = fork();
  
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
	
    int rm = rmdir(table_path);

	strcpy(msg, "TABLE DROPPED.");
	ptr = msg;
    writelog(str, login.id);
    
	return ptr;
}

int main(int argc, char const *argv[]) {
    
    mk_folder("databases");

    pid_t pid, sid;
    pid = fork();

    if (pid < 0) {
            exit(EXIT_FAILURE);
    }
    
    if (pid > 0) {
            exit(EXIT_SUCCESS);
    }
    
    umask(0);

    sid = setsid();
    if (sid < 0) {
            exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }
    
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

	open("/dev/null", O_RDONLY);
	open("/dev/null", O_RDWR);
	open("/dev/null", O_RDWR);

    char buffer[BUFF] = {0}, msg[BUFF] = {};
    
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons( PORT );
    
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))< 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    int *new_sock = malloc(1);
	*new_sock = new_socket;
    
    reconnect();
       
    while (1) {
        bzero(buffer, BUFF);
        valread = read(new_socket, buffer, BUFF);
        
        if (!valread)
        {
            acc = 0;
            isRoot = 0;
            reconnect();
            continue;
        }

        if (buffer[strlen(buffer) - 1] != ';')
        {
            strcpy(msg, "SYNTAX ERROR!");
        }
        else if (!strncmp(buffer, "CREATE USER", 11))
        {
            if (isRoot)
            {
                strcpy(msg, create_user(buffer));
            }
            else
            {
                strcpy(msg, "COMMAND DENIED!");
            }
        }
        
        else if (!strncmp(buffer, "GRANT PERMISSION", 16))
        {
            if (isRoot)
            {
                strcpy(msg, grant_permission(buffer));
            }
            else
            {
                strcpy(msg, "COMMAND DENIED");
            }
        }
        else if (!strncmp(buffer, "USE", 3))
        {
            strcpy(msg, use(buffer));
        }
		else if (!strncmp(buffer, "CREATE DATABASE", 15))
		{
			strcpy(msg, create_database(buffer));
		}
		else if (!strncmp(buffer, "DROP DATABASE", 13))
		{
			strcpy(msg, drop_database(buffer));
		}
		else if (!strncmp(buffer, "CREATE TABLE", 12))
		{
			strcpy(msg, create_table(buffer));
		}
        else if (!strncmp(buffer, "DROP TABLE", 10))
		{
			strcpy(msg, drop_table(buffer));
		}
        else
        {
            strcpy(msg, "QUERY NOT AVAILABLE!");
        }
        send(new_socket, msg, strlen(msg), 0);
    }

    return 0;
}
