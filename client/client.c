#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#define PORT 8080
#define SIZE 100
#define BUFF 1024
int sock = 0, valread, auth_ = 0, uid;
char userlog[SIZE]={};
char uname[BUFF], msg_uid[BUFF];

int main(int argc, char const *argv[]) {

    // === BASE
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[BUFF] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
    
    memset(&serv_addr, '0', sizeof(serv_addr));
    
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    uid=getuid();
    sprintf(msg_uid, "%d", uid);
    send(sock, msg_uid, strlen(msg_uid), 0);

    bool is_root;

    if(uid) is_root=false;
    else if(!uid) is_root=true;

    if(!is_root)
    {
        read(sock, buffer, 1024); 
        bzero(buffer, 1024);
        sprintf(buffer, "%s:%s\n", argv[2], argv[4]);
        send(sock, buffer, strlen(buffer), 0);
    }

    if(is_root) strcpy(uname, "Root");
    else strcpy(uname, argv[2]);

    bzero(buffer, 1024);
    read(sock, buffer, 1024);
    
    printf("%s\n", buffer);
    
    if (strcmp(buffer, "Invalid Username or Password!") == 0) {
            return 0;
    }
    char query[255];

     while (1) 
    {
        printf("ACCESSED BY [%s]> ", uname);
        scanf(" %[^\n]", query);
        send(sock, query, strlen(query), 0);

        bzero(buffer, 1024);
        read(sock, buffer, 1024);

        printf("%s\n", buffer);
    }

    return 0;

}
